<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress2019' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'aurora' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l,6_9eK+8iC+s>e)2t-$8h# %p2gxsub;F]y.?DQU%3X=af#!7? $iDrr::q1?#<' );
define( 'SECURE_AUTH_KEY',  'UZ8!213&a3}!>p5$SPu_DrJ9sOpdntbxeLfj.D&qEcLNwB&kk;0__4K8aIa4oY4m' );
define( 'LOGGED_IN_KEY',    'rhZAmcD2}aT#74IM}r?BH@7RgHs?iJ!2R~E O],gYUgXR#`8QaOE.>KlYt1=DS5R' );
define( 'NONCE_KEY',        '1U7*:^,?MubF6|.EqenRRnb.|Z;QMVM;mD4tolJmr:rb),.`!$W1`R15hq$YKy1M' );
define( 'AUTH_SALT',        '/,<3U{<NW7ewdX}yuk3OjC}u:%)JXr +wA@9dIfR3C%5oifSp? Khb(l.8%khyX-' );
define( 'SECURE_AUTH_SALT', ',f}~OR/>DfVY@0GLq%3aB% W _]J&i_J,Z4HR*9[8Q@HXc$=ANr/`[XL+%1!-5ea' );
define( 'LOGGED_IN_SALT',   'akQN,!rt+tXn-w&3(LWSW8LC?<G2NghScfk|1i#B6mO!A=v_WcC(P6q7,fHBcJLz' );
define( 'NONCE_SALT',       'E@< V,gCM%gb$u1&0V9>fviJN}e!%|2IZWb,=WZ$R24%^`+%qr]g#<t(d^1HvhFR' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'SAVEQUERIES', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
