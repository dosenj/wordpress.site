<?php
/**
* Plugin Name: Task
* Plugin URI: http://wordpress.site/plugins/task/
* Description: This is Task plugin of wordpress.site 2019
* Version: 1.0
* Requires at least: 5.2
* Requires PHP: 7.1
* Author: Jovan Dosen
* Author URI: https://jovandosen.com/
* License: GPL v2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: task
* Domain Path: /languages

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

if( !defined('ABSPATH') ){
	exit();
}

require 'include/Foo.php';
require 'include/Bar.php';
require 'include/Baz.php';
require 'include/AppAssets.php';

class Task
{
	public function __construct()
	{
		add_action('plugins_loaded', array($this, 'boot'));
	}

	public function boot()
	{
		$foo = new Foo(); // custom post type movies
		$bar = new Bar(); // movie taxonomy ( genre )
		$baz = new Baz(); // custom meta box 
		$assets = new AppAssets(); // assets ( css, js )
	}

	public static function create()
	{
		return new self();
	}
}

Task::create();

?>