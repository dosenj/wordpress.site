<?php

class Baz
{
	public function __construct()
	{
		add_action('add_meta_boxes', array($this, 'addBoxData'));
		add_action('save_post', array($this, 'saveBoxData'));
	}

	public function addBoxData()
	{
		$screens = ['post', 'movies'];
	    foreach ($screens as $screen) {
	        add_meta_box(
	            'box_id',           
	            'Custom Meta Box Title',  
	            array($this, 'addBoxHtml'),  
	            $screen                   
	        );
	    }
	}

	public function addBoxHtml($post)
	{
		$value = get_post_meta($post->ID, 'box_id', true);
	    ?>
	    <label for="data">Description for this field</label>
	    <input type="text" name="data" id="data" value="<?php echo $value; ?>" style="width: 300px;">
	    <?php
	}

	public function saveBoxData($post_id)
	{
		if (array_key_exists('data', $_POST)) {
	        update_post_meta(
	            $post_id,
	            'box_id',
	            $_POST['data']
	        );
	    }
	}
}

?>