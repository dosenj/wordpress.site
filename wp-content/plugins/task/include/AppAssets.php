<?php

class AppAssets
{
	public function __construct()
	{
		add_action('wp_enqueue_scripts', array($this, 'loadAppAssets'));
	}

	public function loadAppAssets()
	{
		wp_register_script('js', plugins_url('/task/assets/js/app.js'), array('jquery'),'1.1', true);
		wp_enqueue_script('js');

		wp_register_style('css', plugins_url('/task/assets/css/app.css'));
		wp_enqueue_style('css');
	}
}

?>