<?php /* Template Name: Custom Example Template */ ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>
			Custom page template
		</title>
		<meta charset="utf-8">
		<meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>

		<?php get_header(); ?>

		<div class="container">
			<h1>Custom page template data</h1>
			<div class="page-data">
				<?php

					if(have_posts()){
						while(have_posts()){
							the_post();
							the_title();
							the_content();
						}
					}

				?>
			</div>
		</div>

		<?php get_footer(); ?>

		<?php wp_footer(); ?>
	</body>
</html>