<?php

function addFeatures()
{
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'automatic-feed-links' );
}

add_action('after_setup_theme', 'addFeatures');

function loadDevThemeAssets()
{
	wp_enqueue_style('devcss', get_template_directory_uri() . '/css/dev.css', false, '1.1', 'all');
	wp_enqueue_script('devjs', get_template_directory_uri() . '/js/dev.js', array('jquery'), 1.1, true);
}

add_action('wp_enqueue_scripts', 'loadDevThemeAssets');

function my_menu() 
{
    add_options_page(
        'My Options Foo',
        'My Menu Foo',
        'manage_options',
        'my-unique-identifier',
        'my_options'
    );
}

function my_options() 
{
    if ( !current_user_can( 'manage_options' ) ) {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    echo 'Here is where I output the HTML for my screen. Well and good';
    echo '</div><pre>';
}

add_action( 'admin_menu', 'my_menu' );

function register_my_theme_settings_menu() 
{
    add_menu_page(
        "My Theme's Settings Bar",
        "My Theme Bar",
        "manage_options",
        "my-theme-settings-menu"
    );
}
 
function register_my_theme_more_settings_menu() 
{
    add_submenu_page(
        "my-themes-settings-menu",
        "More Settings for My Theme Bar",
        "More Settings Bar",
        "manage_options",
        "my-theme-more-settings-menu"
    );
}
 
add_action( "admin_menu", "register_my_theme_settings_menu");
add_action( "admin_menu", "register_my_theme_more_settings_menu");

function dev_widgets_init() 
{
    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'dev' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Secondary Sidebar', 'dev' ),
        'id'            => 'sidebar-2',
        'before_widget' => '<ul><li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li></ul>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}

add_action('widgets_init', 'dev_widgets_init');

function register_my_menus() 
{
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
     )
   );
 }

 add_action( 'init', 'register_my_menus' );

 function addCustomizerData($wp_customize)
 {
 	$wp_customize->add_setting('simple_input_text_field', array(
        'default' => 'Simple text to display'
    ));

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'simple_control_id',
        array(
            'label' => __('Simple label text', 'predator'),
            'section' => 'simple_section',
            'settings' => 'simple_input_text_field'
        )
    ));

    $wp_customize->add_section('simple_section', array(
        'title' => __('Simple section title'),
        'description' => __('Simple section description'),
        'panel' => 'simple_panel'
    ));

    $wp_customize->add_panel('simple_panel', array(
        'title' => __('Simple panel title of Dev theme 2019'),
        'description' => __('Simple panel description'),
        'priority' => 160
    ));	

    // New section

    $wp_customize->add_setting('extra_text_field', array(
        'default' => 'Extra text field data'
    ));

    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'extra_text_field_control',
        array(
            'label' => __('Extra text field label', 'predator'),
            'section' => 'extra_section',
            'settings' => 'extra_text_field'
        )
    ));

    $wp_customize->add_section('extra_section', array(
        'title' => __('Extra section AREA'),
        'description' => __('Extra section description'),
        'panel' => 'extra_panel'
    ));

    $wp_customize->add_panel('extra_panel', array(
        'title' => __('Extra panel AREA'),
        'description' => __('Extra panel description'),
        'priority' => 200
    ));

    $wp_customize->add_setting('extra_image');

    $wp_customize->add_control(new WP_Customize_Upload_Control(
        $wp_customize,
        'extra_image_control',
        array(
            'label' => __('Extra image control'),
            'section' => 'extra_section',
            'settings' => 'extra_image'
        )
    ));

 }

 add_action('customize_register', 'addCustomizerData');

?>