<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Movies details</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>
		<?php get_header(); ?>

		<h1>All Movies data:</h1>

		<div class="container-for-movies">
			
			<?php

				if(have_posts()){
					while(have_posts()){
						the_post(); // get the post/page data
						the_title(); // page title
						the_content(); // page content
						the_post_thumbnail();
						?>
						<p><a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a></p>
						<?php
					}
					?>
					<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>	
					<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
					<?php
					the_posts_pagination();
				} else {
					echo "No post data found";
				}

			?>

		</div>

		<?php wp_footer(); ?>
	</body>
</html>