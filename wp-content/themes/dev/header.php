<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Index file of Dev theme</title>
		<meta charset="utf-8">
		<?php wp_head(); ?>
	</head>
	<body>
		<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>

		<?php wp_nav_menu( array( 'theme_location' => 'extra-menu' ) ); ?>