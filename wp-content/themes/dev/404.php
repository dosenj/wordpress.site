<!DOCTYPE html>
<html lang="en">
	<head>
		<title>404 Not found</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	<body>

		<?php get_header(); ?>

		<div class="container">
			<?php
				echo "This page does not exist!";
				get_search_form();
			?>
		</div>

		<?php get_footer(); ?>

		<?php wp_footer(); ?>
	</body>
</html>